import unittest
import Rectangulo #Importo el nombre del fichero donde tengo los datos

#La funcion siempre tiene que empezar por test para que funcione
class RectanguloTestCase(unittest.TestCase):
    def test_perimetro(self):
        rectangulo1 = Rectangulo.Rectangulo(1, 2)
        perimetro1 = rectangulo1.perimetro()
        self.assertEqual(perimetro1, 6)


unittest.main()