class Empleado:
    def __init__(self, nombre, salario, tasa, antiguedad):
        self.__nombre = nombre
        self.__salario = salario
        self.__tasa = tasa
        self.__antiguedad = antiguedad
        #el self es a quien me estan apuntando
        #la clase es empleado mientras que las instancias/objetos son ana,pepe,...
        #el init es el constructor, que es un método
        #una clase es una forma de construir plantillas que me permiten crear nuevas definiciones de cosas que voy a necesitar
    def CalculoImpuestos(self):#aqui antes del :.2f pone tax, que es el nombre del "hueco", mas tarde especificamos que tiene que ir en ese hueco
        self.__impuestos = self.__salario*self.__tasa
        print ("El empleado {name} debe pagar {tax:.2f}".format(name=self.__nombre, tax=self.__impuestos))
        return self.__impuestos
    def antiguedad(self):
        if (self.__antiguedad > 1):
            rebaja = (self.__impuestos)*0.1
            self.__impuestos = self.__impuestos -  rebaja
        return print("Su precio tras la rebaja es", self.__impuestos)
     
def displayCost(total): #:.2f significa 2 float, que printee 2 decimales
    print("Los impuestos a pagar en total son {:.2f} euros".format(total))
emp1 = Empleado("Pepe", 20000, 0.35, 2)
emp2 = Empleado("Ana", 30000, 0.30, 0)
empleados = [emp1, emp2, Empleado("Luis", 10000, 0.10, 4), Empleado("Luisa", 25000, 0.15, 0)]
total = 0
for emp in empleados:
    total += emp.CalculoImpuestos()
    rebaja = emp.antiguedad()

displayCost(total)