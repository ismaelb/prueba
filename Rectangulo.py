class Rectangulo:
    def __init__(self, base, altura):
        self.base = base
        self.altura = altura

    def area(self):
        return (self.base * self.altura)
    def perimetro(self):
        return ((self.base * 2) + (self.altura * 2))
    
rectangulo1 = Rectangulo(2, 4)
rectangulo2 = Rectangulo(3, 9)

print("El perimetro del rectangulo es", rectangulo1.perimetro(), "y el area del rectangulo es", rectangulo1.area())
