class Cuadrado:
    """Un ejemplo de una clase para los Cuadrados"""
    #A la hora de definir una clase me tengo que preguntar ¿que tiene? y ¿que hace?
    #si no se introduce ningun valor para el lado, se tomara el valor por defecto 1
    def __init__(self,l=1):
        self.lado = l
        self.miarea = self.lado**2

    def area(self):
        return self.miarea
    def perimetro(self):
        return (self.lado*4)

cuadrado01 = Cuadrado(2)
cuadrado02 = Cuadrado()
print(cuadrado01.perimetro())
    
